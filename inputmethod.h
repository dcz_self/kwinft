/*
    KWin - the KDE window manager
    This file is part of the KDE project.

    SPDX-FileCopyrightText: 2016 Martin Gräßlin <mgraesslin@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/
#ifndef KWIN_VIRTUAL_KEYBOARD_H
#define KWIN_VIRTUAL_KEYBOARD_H


#include <kwin_export.h>

#include <QObject>

#include <kwinglobals.h>

#include <QPointer>


class KStatusNotifierItem;

namespace Wrapland
{
namespace Server
{
class Client;
class InputMethodV2;
class Surface;
class TextInputV3;
}
}

using Wrapland::Server::Client;
using Wrapland::Server::InputMethodV2;
using Wrapland::Server::Surface;
using Wrapland::Server::TextInputV3;

namespace KWin
{

/**
 * This class connectss input_method_unstable_v2, and text_input_unstable_v3
 * to provide full input method support, both to input methods,
 * and to applications.
 **/
class KWIN_EXPORT InputMethod : public QObject
{
    Q_OBJECT
public:
    ~InputMethod() override;

    void init();
    void setEnabled(bool enable);
    bool isEnabled() const {
        return m_enabled;
    }
    bool isActive() const {
        return m_active;
    }
    void setActive(bool active);
    void hide();
    void show();

Q_SIGNALS:
    void activeChanged(bool active);
    void enabledChanged(bool enabled);

private Q_SLOTS:
    void inputMethodAdded(Wrapland::Server::InputMethodV2 *client);

    // textinput interface slots
    void handleFocusedSurfaceChanged();
    void surroundingTextChanged();
    void contentTypeChanged();
    void textInputInterfaceV2EnabledChanged();
    void textInputInterfaceV3EnabledChanged();
    void stateCommitted(uint32_t serial);

    // inputmethod slots
    void setPreeditString(uint32_t serial, const QString &text, const QString &commit);

private:
    void updateSni();
    void commitString(qint32 serial, const QString &text);
    void deleteSurroundingText(int32_t index, uint32_t length);

    struct {
        QString text = QString();
        quint32 begin = 0;
        quint32 end = 0;
    } preedit;

    // ???
    bool m_enabled = false;
    // ???
    bool m_active = false;
    KStatusNotifierItem *m_sni = nullptr;
    QPointer<InputMethodV2> m_inputMethod;
    // Tracks previous text input, so that commit signal can be disconnected.
    QPointer<TextInputV3> m_trackedTextInput;
    QPointer<Surface> m_trackedSurface;
//    QList<TextInputV3> m_textInputs;

    KWIN_SINGLETON(InputMethod)
};

}

#endif
